package ex1;

public class Mystack {
	private final int InitCapacity=10;
	private Integer []data;
	private Integer []mindata;
	private int index;
	private int capacity;
	
	public Mystack() {
		data=new Integer[InitCapacity];
		mindata=new Integer[InitCapacity];
		index=-1;
		capacity=InitCapacity;
	}
	void PushStack(Integer elem) {//实现题目的Push
		index++;
		if(index>capacity-1) {//如果栈满了，就扩容
			Integer []newData=new Integer[capacity*2];
			Integer []newMindata=new Integer[capacity*2];
			for(int i=0;i<capacity;i++) {
				newData[i]=data[i];
				newMindata[i]=mindata[i];
			}
			data=newData;
			mindata=newMindata;
		}
		data[index]=elem;
		if(index==0) {
			mindata[0]=elem;
		}else{
			if(mindata[index-1]>elem) {
				mindata[index]=elem;
			}else {
				mindata[index]=mindata[index-1];
			}
		}
	}
	Integer PopStack() { //实现题目的POP
		if(index<0) {
			return null;
		}
		return data[index--];
	}
	int GetStackLength() {
		return index+1;
	}

	Integer GetMin() {//实现题目中的O(1)复杂度的getmin
		if(index<0) {
			return null;
		}
		return mindata[index];
	}
	void PrintStack() {
		System.out.print("PrintStack():");
		for(int i=0;i<=index;i++) {
			System.out.print(data[i]+" ");
		}
		System.out.print("\nmin():");
		for(int i=0;i<=index;i++) {
			System.out.print(mindata[i]+" ");
		}
	}
}
