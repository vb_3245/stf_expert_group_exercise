/*
第二题
编写一个类，用两个栈实现队列，支持队列的基本操作（add，poll，peek）
 1. add是在队列中添加元素
 2. Poll是弹出元素
 3. Peek是查询队首的元素，但不删除元素
*/
#include <stdio.h>
#include "queue.h"
int main(int argc, char **argv)
{
	myqueue q=InitQueue();
	int element;
	AddQueue(&q,2);
	AddQueue(&q,3);
	PollQueue(&q,&element);
	printf("%d\n",element);
	AddQueue(&q,10);
	PeekQueue(&q,&element);
	printf("%d\n",element);
	return 0;
}
