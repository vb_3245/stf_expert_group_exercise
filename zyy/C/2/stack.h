#ifndef STACK_H
#define STACK_H
#define INITLEN 20

typedef int elem;//根据你的需求可以定义成其他类型,如float,double,C++和java中可使用模板，泛型

typedef struct stack{
	elem* data;//原始数据 
	elem* mindata;//存最小值 
	int index;//指向最后一个元素的索引
	int capacity;//当前栈的容量 
}mystack;

mystack InitStack();
int PushStack(mystack*,elem);//1代表插入成功,0代表插入失败，实现题目的Push 
elem PopStack(mystack*,elem *); //实现题目的POP 
int GetStackLength(mystack*);
void DeleteStack(mystack*);
elem GetMin(mystack*,elem *);//实现题目中的O(1)复杂度的getmin
void PrintStack(mystack*);
int GetStackTop(mystack*,elem*);
#endif
