#ifndef QUEUE_H
#define QUEUE_H
#include "stack.h"

typedef struct myqueue{
    mystack a;//用于塞入元素
    mystack b;//取的时候发现没有，则从a压入
}myqueue;

myqueue InitQueue();
int PollQueue(myqueue *this,int *elem);
void AddQueue(myqueue *this,int elem);
void AConvertToB(myqueue *this);
int PeekQueue(myqueue *this,int *elem);
#endif