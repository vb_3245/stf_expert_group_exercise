#include "queue.h"
#include <stdio.h>
myqueue InitQueue()
{
    myqueue q;
    q.a=InitStack();
    q.b=InitStack();
    return q;
}

void AddQueue(myqueue *this,int elem)
{
    PushStack(&this->a,elem);
}

//先查询b是否为空
//为空则从a导入
//导入后，b依然为空，则返回
int PollQueue(myqueue *this,int *elem)
{
    if(GetStackLength(&this->b)<=0)
    {
        if(GetStackLength(&this->a)>0)
        {
            AConvertToB(this);
        }
    }
    if(GetStackLength(&this->b)>0)
    {
        PopStack(&this->b,elem);
        return 1;
    }
    else
    {
        return 0;
    }
}

void AConvertToB(myqueue *this)
{
    int temp,i;
    int len=GetStackLength(&this->a);
    for(i=0;i<len;i++)
    {
        PopStack(&this->a,&temp);
        PushStack(&this->b,temp);
    }
}

int PeekQueue(myqueue *this,int *elem)
{
    if(GetStackLength(&this->b)<=0)
    {
        if(GetStackLength(&this->a)>0)
        {
            AConvertToB(this);
        }
    }
    if(GetStackLength(&this->b)>0)
    {
        GetStackTop(&this->b,elem);
        return 1;
    }
    else
    {
        return 0;
    }
}