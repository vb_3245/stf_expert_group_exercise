#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
	mystack stack = InitStack();//初始化栈
	PushStack(&stack,5);
	PushStack(&stack,6);
	PushStack(&stack,4);
	PushStack(&stack,6);
	PushStack(&stack,6);
	PushStack(&stack,6);
	PushStack(&stack,1);
	//打印栈状态
	PrintStack(&stack);
	//获取最小值 
	int data;
	GetMin(&stack,&data);
	printf("The min is %d\n",data);
	//弹出最后一个1 
	PopStack(&stack,&data);
	printf("Pop():%d\n",data);
	//打印栈状态 
	PrintStack(&stack);
	DeleteStack(&stack); 
	return 0;
}
