import java.util.Random;
import java.util.Stack;

public class y20211228 {
    public static void main(String[] args) {
        Random rd = new Random();
        MyQueue q = new MyQueue();
        for (int i = 0; i < 5; i++) {
            q.add(rd.nextInt(10));
        }
        q.poll();
        System.out.println("peek = " + q.peek());
        q.add(2);
        q.poll();
        System.out.println("peek = " + q.peek());
    }
}

class MyQueue {
    private final Stack<Integer> stackA;
    private final Stack<Integer> stackB;

    public MyQueue() {
        this.stackA = new Stack<>();
        this.stackB = new Stack<>();
    }

    public void add(int i) {
        System.out.println("Queue add " + i);
        stackA.push(i);
    }

    public void poll() {
        if (!stackA.empty()) {
            System.out.println("Queue polled");
            while (!stackA.empty()) {
                stackB.push(stackA.peek());
                stackA.pop();
            }
            stackB.pop();
            while (!stackB.empty()) {
                stackA.push(stackB.peek());
                stackB.pop();
            }
        }
    }

    public int peek() {
        if (!stackA.empty()) {
            while (!stackA.empty()) {
                stackB.push(stackA.peek());
                stackA.pop();
            }
            int t = stackB.peek();
            while (!stackB.empty()) {
                stackA.push(stackB.peek());
                stackB.pop();
            }
            return t;
        } else {
            return -1;
        }
    }
}