import java.util.Random;
import java.util.Stack;

public class y20211222 {

    public static void main(String[] args) {
        Random rd = new Random();
        MyStack myStack = new MyStack();
        for (int i = 0; i < 50; i++) {
            myStack.push(rd.nextInt(10));
            System.out.println("top:" + myStack.top());
            System.out.println("min:" + myStack.getMin());
        }
        myStack.pop();
        myStack.pop();
        System.out.println("------------");
        System.out.println("top:" + myStack.top());
        System.out.println("min:" + myStack.getMin());

    }
}

class MyStack {
    private final Stack<Integer> source;
    private final Stack<Integer> min;

    public MyStack() {
        this.source = new Stack<>();
        this.min = new Stack<>();
    }

    void push(int i) {
        source.push(i);
        if (min.isEmpty() || min.peek() >= i)
            min.push(i);
    }

    void pop() {
        int t = source.pop();
        if (t <= min.peek())
            min.pop();
    }

    int top() {
        return source.peek();
    }

    int getMin() {
        return min.peek();
    }
}
