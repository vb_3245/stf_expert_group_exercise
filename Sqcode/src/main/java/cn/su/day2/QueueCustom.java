package cn.su.day2;

import cn.su.day1.StackCustom;
import lombok.Data;

/**
 * @Description:
 * 编写一个类，用两个栈实现队列，支持队列的基本操作（add，poll，peek）
 *
 * add是在队列中添加元素
 * poll是弹出元素
 * peek是查询队首的元素，但不删除元素
 * @author: Sqcode
 * @since: 2021/12/24 13:46
 */
@Data
public class QueueCustom {

    /** 实现栈 顺序*/
    private StackCustom stack;
    /** 实现队列 顺序 */
    private StackCustom queue;

    public QueueCustom() {
        init(0);
    }

    public QueueCustom(int len) {
        init(len);
    }

    void init(int len) {
        stack = new StackCustom(len);
        queue = new StackCustom(len);
    }

    public void add(int value){
        stack.push(value);
        boolean empty = queue.isEmpty();
        if (empty) {
            toQueue();
        }
    }

    public int poll(){
        int pop = queue.pop();
        if (queue.isEmpty()) {
            toQueue();
        }
        return pop;
    }

    public int peek(){
        return queue.getStk()[queue.getRealLength()-1];
    }

    private void toQueue(){
        int length = stack.getRealLength();
        for (int i = 0; i < length; i++) {
            int pop = stack.pop();
            queue.push(pop);
        }
    }
}
