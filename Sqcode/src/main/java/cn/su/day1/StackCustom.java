package cn.su.day1;

import lombok.Data;

import java.util.Arrays;

/**
 * @Description:
 * 2021.12.22
 * 设计一个有getMin功能的栈
 * 实现一个特殊的栈，在实现栈的基本功能上，再实现返回栈中最小元素的操作。
 * 要求：
 * 1.pop,push,getMin操作的时间复杂度都是O(1)
 * 2. 初始化长度 5 , 不足2倍扩容
 * @author: Sqcode
 * @since: 2021/12/22 10:52
 */
@Data
public class StackCustom {

    /** 保存栈的格式，后进先出 */
    private Integer[] stk;
    /** 保存 最小值 */
    private Integer[] mins;
    /** 数组扩容长度 */
    private Integer count = 0;
    /** 当前值下标，0开始*/
    private Integer index = 0;

    public StackCustom() {
        init (0);
    }

    public StackCustom(int len) {
        init (len);
    }

    void init (int len) {
        if (stk == null) {
            stk = new Integer[len];
        }
        if (mins == null) {
            mins = new Integer[len];
        }
        count = len;
    }

    /**
        2个数组，长度是一致的
     */
    void capacity(int len) {
        stk = Arrays.copyOf(stk, stk.length + len);
        mins = Arrays.copyOf(mins, mins.length + len);
        count = stk.length;
        System.out.println("扩容："+count);
    }

    /**
     *
     * @param value
     */
    public void push (int value) {
        if (index.equals(count)) {
            capacity(count);
        }

        if (index == 0) {
            stk[0] = value;
            mins[0] = value;
        } else {
            stk[index] = value;
            int min = mins[index-1];
            if (value < min) {
                mins[index] = value;
            } else {
                mins[index] = min;
            }
        }
        index++;
    }

    /**
     *
     * @return
     */
    public int pop () {
        index--;
        int value = stk[index];
        stk[index] = null;
        mins[index] = null;
        return value;
    }

    /**
     *
     * @return
     */
    public int getMin () {
        return mins[index-1];
    }

    public boolean isEmpty() {
        return getRealLength() == 0 ? true : false;
    }

    public int getRealLength(){
        int count = 0;
        for (int i = 0; i < stk.length; i++) {
            if (stk[i] != null) {
                count += 1;
            }
        }
        return count;
    }
}
