package cn.su.day1;

import java.util.Arrays;
import java.util.Random;

/**
 * @Description:
 * @author: Sqcode
 * @since: 2021/12/24 13:50
 */
public class Test1 {

    public static void main(String[] args) {
        int times = 2;

        StackCustom stackCustom = new StackCustom(times);

        Random random = new Random();
        for (int i = 1; i <= times; i++) {
            stackCustom.push(random.nextInt(100) + 1);
        }

        for (int i = 1; i <= times; i++) {
            System.out.println(String.format("%s, %s", Arrays.toString(stackCustom.getStk()), Arrays.toString(stackCustom.getMins())));
            System.out.println("最小值：" + stackCustom.getMin());
            System.out.println("pop：" + stackCustom.pop());
            System.out.println();
        }
    }

}
