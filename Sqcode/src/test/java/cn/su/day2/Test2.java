package cn.su.day2;

import cn.su.day1.StackCustom;

import java.util.Arrays;
import java.util.Random;

/**
 * @Description:
 * @author: Sqcode
 * @since: 2021/12/24 13:55
 */
public class Test2 {

    public static void main(String[] args) {
        int len = 3;

        QueueCustom queueCustom = new QueueCustom(len);
        Random random = new Random();

        for (int i = 1; i <= len; i++) {
            int value = random.nextInt(100) + 1;
            queueCustom.add(value);
        }

        for (int i = 1; i <= len + 1; i++) {
            System.out.println(String.format("%s, %s", Arrays.toString(queueCustom.getStack().getStk()), Arrays.toString(queueCustom.getQueue().getStk())));
            System.out.println("peek：" + queueCustom.peek());
            System.out.println("pop：" + queueCustom.poll());

            int value = random.nextInt(100) + 1;
            queueCustom.add(value);

            System.out.println();
        }
    }

}
